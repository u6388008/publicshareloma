(function ($) {
  var myBlog = [
    {
      pic1: "img1.png",
      pic2: "img2.png",
      pic3: "img3.png",
      pic4: "img4.png",
      pic5: "img5.png",
      img1id: "img1-1",
      img2id: "img1-2",
      img3id: "img1-3",
      img4id: "img1-4",
      img5id: "img1-5",
      img1dot: "dot1-1",
      img2dot: "dot1-2",
      img3dot: "dot1-3",
      img4dot: "dot1-4",
      img5dot: "dot1-5",
      slide: "slide1",
      title: "พระผุด วัดพระทอง ภูเก็ต",
      like: "1,230",
      view: "1,230",
      text:
        "วัดพระทองเป็นวัดเก่าแก่ที่สร้างในสมัยกรุงศรีอยุธยา ประดิษฐานพระพุทธรูปศักดิ์สิทธิ์ พระทอง หรือ พระผุด เป็นพระประธานในวิหาร",
      link: "#",
      activity: "2 กิจกรรม"
    },
    {
      pic1: "img2.png",
      pic2: "img3.png",
      pic3: "img4.png",
      pic4: "img5.png",
      pic5: "img6.png",
      img1id: "img2-1",
      img2id: "img2-2",
      img3id: "img2-3",
      img4id: "img2-4",
      img5id: "img2-5",
      img1dot: "dot2-1",
      img2dot: "dot2-2",
      img3dot: "dot2-3",
      img4dot: "dot2-4",
      img5dot: "dot2-5",
      slide: "slide2",
      title: "พิพิธภัณฑ์บัว ปทุมธานี",
      like: "1,230",
      view: "1,230",
      text:
        "พิพิธภัณฑ์บัว มีชื่อเต็มๆว่า พิพิธภัณฑ์บัวมหาวิทยาลัยเทคโนโลยีราชมงคลธัญบุรี ตั้งอยู่ที่มหาวิทยาลัยเทคโนโลยีราชมงคลธัญบุรี",
      link: "#",
      activity: "0 กิจกรรม"
    },
    {
      pic1: "img3.png",
      pic2: "img4.png",
      pic3: "img5.png",
      pic4: "img6.png",
      pic5: "img7.png",
      img1id: "img3-1",
      img2id: "img3-2",
      img3id: "img3-3",
      img4id: "img3-4",
      img5id: "img3-5",
      img1dot: "dot3-1",
      img2dot: "dot3-2",
      img3dot: "dot3-3",
      img4dot: "dot3-4",
      img5dot: "dot3-5",
      slide: "slide3",
      title: "พิพิธภัณฑ์เปลือกหอย (Phuke...",
      like: "1,230",
      view: "1,230",
      text:
        "วัดพระทองเป็นวัดเก่าแก่ที่สร้างในสมัยกรุงศรีอยุธยา ประดิษฐานพระพุทธรูปศักดิ์สิทธิ์ พระทอง หรือ พระผุด เป็นพระประธานในวิหาร",
      link: "#",
      activity: "1 กิจกรรม"
    },
    {
      pic1: "img4.png",
      pic2: "img5.png",
      pic3: "img6.png",
      pic4: "img7.png",
      pic5: "img8.png",
      img1id: "img4-1",
      img2id: "img4-2",
      img3id: "img4-3",
      img4id: "img4-4",
      img5id: "img4-5",
      img1dot: "dot4-1",
      img2dot: "dot4-2",
      img3dot: "dot4-3",
      img4dot: "dot4-4",
      img5dot: "dot4-5",
      slide: "slide4",
      title: "สวนนกภูเก็ต ภูเก็ต",
      like: "1,230",
      view: "1,230",
      text:
        "สวนนกภูเก็ตมีพื้นที่กว่า 20 ไร่ ใกล้ตัวเมืองภูเก็ต ภายในสวนนกภูเก็ตมีนกหลากหลายสายพันธุ์ มากกว่า 80 ชนิด ทั้งนกที่สามารถ...",
      link: "#",
      activity: "1 กิจกรรม"
    },
    {
      pic1: "img5.png",
      pic2: "img6.png",
      pic3: "img7.png",
      pic4: "img8.png",
      pic5: "img9.png",
      img1id: "img5-1",
      img2id: "img5-2",
      img3id: "img5-3",
      img4id: "img5-4",
      img5id: "img5-5",
      img1dot: "dot5-1",
      img2dot: "dot5-2",
      img3dot: "dot5-3",
      img4dot: "dot5-4",
      img5dot: "dot5-5",
      slide: "slide5",
      title: "หาดกะตะน้อย ภูเก็ต",
      like: "1,230",
      view: "1,230",
      text:
        "หาดกะตะน้อยเป็นหนึ่งในสามหาดที่อยู่ติดกันกับหาดกะตะและหาดกะรน อยู่ทางทิศใต้ของหาดกะตะ หาดกะตะน้อยเป็นหาดที่เล็ก...",
      link: "#",
      activity: "0 กิจกรรม"
    },
    {
      pic1: "img6.png",
      pic2: "img7.png",
      pic3: "img8.png",
      pic4: "img9.png",
      pic5: "img1.png",
      img1id: "img6-1",
      img2id: "img6-2",
      img3id: "img6-3",
      img4id: "img6-4",
      img5id: "img6-5",
      img1dot: "dot6-1",
      img2dot: "dot6-2",
      img3dot: "dot6-3",
      img4dot: "dot6-4",
      img5dot: "dot6-5",
      slide: "slide6",
      title: "หาดกะรน ภูเก็ต",
      like: "1,230",
      view: "1,230",
      text:
        "หาดกะรนอยู่ทางทิศใต้ของหาดป่าตอง ตั้งอยู่ติดกันกับหาด กะตะ และหาดกะตะน้อย โดยรวมถึงเรียกรวมๆบริเวณนี้ว่าสามหาด หาก...",
      link: "#",
      activity: "1 กิจกรรม"
    },
    {
      pic1: "img7.png",
      pic2: "img8.png",
      pic3: "img9.png",
      pic4: "img1.png",
      pic5: "img2.png",
      img1id: "img7-1",
      img2id: "img7-2",
      img3id: "img7-3",
      img4id: "img7-4",
      img5id: "img7-5",
      img1dot: "dot7-1",
      img2dot: "dot7-2",
      img3dot: "dot7-3",
      img4dot: "dot7-4",
      img5dot: "dot7-5",
      slide: "slide7",
      title: "Pattaya Dolphin World (โลมาโชว์ พัทยา) สัตหีบ ชลบุรี",
      like: "1,230",
      view: "1,230",
      text:
        "ในสมัยก่อนการได้มาเที่ยวชมโชว์ปลาโลมาและกิจกรรมว่ายน้ำกับปลาโลมามักนึกถึง โอเอซิส ซีเวิลด์ (Oasis Sea World)",
      link: "#",
      activity: "1 กิจกรรม"
    },
    {
      pic1: "img8.png",
      pic2: "img9.png",
      pic3: "img1.png",
      pic4: "img2.png",
      pic5: "img3.png",
      img1id: "img8-1",
      img2id: "img8-2",
      img3id: "img8-3",
      img4id: "img8-4",
      img5id: "img8-5",
      img1dot: "dot8-1",
      img2dot: "dot8-2",
      img3dot: "dot8-3",
      img4dot: "dot8-4",
      img5dot: "dot8-5",
      slide: "slide8",
      title: "บ้านกลับหัว (Upside Down Pattaya) สัตหีบ ชลบุรี",
      like: "1,230",
      view: "1,230",
      text:
        "บ้านกลับหัวเป็นสถานที่เที่ยวแนวใหม่ที่เหมาะกับการมาถ่ายภาพเล่นกับมุมกล้องที่บ้านทั้งหลังตีลังกาทั้งจากข้างนอกและข้างใน...",
      link: "#",
      activity: "0 กิจกรรม"
    },
    {
      pic1: "img9.png",
      pic2: "img1.png",
      pic3: "img2.png",
      pic4: "img3.png",
      pic5: "img4.png",
      img1id: "img9-1",
      img2id: "img9-2",
      img3id: "img9-3",
      img4id: "img9-4",
      img5id: "img9-5",
      img1dot: "dot9-1",
      img2dot: "dot9-2",
      img3dot: "dot9-3",
      img4dot: "dot9-4",
      img5dot: "dot9-5",
      slide: "slide9",
      title: "เกาะแสมสาร สัตหีบ ชลบุรี",
      like: "1,230",
      view: "1,230",
      text:
        "เกาะแสมสารเป็นส่วนหนึ่งของโครงการอนุรักษ์พันธุกรรมพืชอันเนื่องมาจากพระราชดำริ สมเด็จพระเทพรัตนราชสุดาฯ...",
      link: "#",
      activity: "1 กิจกรรม"
    }
  ];
  var blog = document.getElementById("myListBlog");
  // ref to id myListBlog
  var innerHTML = "";
  for (let i = 0; i < myBlog.length; i++) {
    let data = myBlog[i];
    innerHTML += `
    <div class="travel-card">
    <ul class="slides">`;

    for (let j = 1; j <= 5; j++) {
      const isChecked = j === 1 ? "checked" : ""; // Check the first input
      innerHTML += `
          <input type="radio" name="radio-btn-${i}" id="${
        data["img" + j + "id"]
      }" ${isChecked} />
          <li class="slide-container">
            <div class="${data.slide}">
              <img src="${data["pic" + j]}" />
            </div>
            <div class="nav">
              <label for="${
                data["img" + (j === 1 ? 5 : j - 1) + "id"]
              }" class="prev">&#x2039;</label>
              <label for="${
                data["img" + (j === 5 ? 1 : j + 1) + "id"]
              }" class="next">&#x203a;</label>
            </div>
          </li>
          
  <style>
  .${data.slide} {
    top: 0;
    opacity: 0;
    width: 360px;
    height: 210px;
    display: flex;
    position: absolute;
    transform: translate(1, 0);

    transition: all 0.7s ease-in-out;
  }

  .${data.slide} img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .${data.slide}:hover + .nav label {
  opacity: 0.5;
  }

  input:checked + .slide-container .${data.slide} {
    opacity: 1;

    transition: opacity 1s ease-in-out;
  }
        input#${data.img1id}:checked ~ .nav-dots label#${data.img1dot},
        input#${data.img2id}:checked ~ .nav-dots label#${data.img2dot},
        input#${data.img3id}:checked ~ .nav-dots label#${data.img3dot},
        input#${data.img4id}:checked ~ .nav-dots label#${data.img4dot},
        input#${data.img5id}:checked ~ .nav-dots label#${data.img5dot} {
          background: var(--other-white, #fff);
        }
      </style>
          
          `;
    }

    innerHTML += `
          <li class="nav-dots">
            ${[1, 2, 3, 4, 5]
              .map(
                (num) =>
                  `<label for="${
                    data["img" + num + "id"]
                  }" class="nav-dot" id="${data["img" + num + "dot"]}"></label>`
              )
              .join("")}
          </li>
        </ul>
        
    <div class="card">
      <div class="first-linecard">
        <h4>${data.title}</h4>

        <div class="likeview">
          <div class="bun" id="like">
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10" fill="none">
              <path d="M5.00065 8.89583L4.39648 8.34583C2.25065 6.4 0.833984 5.1125 0.833984 3.54167C0.833984 2.25417 1.84232 1.25 3.12565 1.25C3.85065 1.25 4.54648 1.5875 5.00065 2.11667C5.45482 1.5875 6.15065 1.25 6.87565 1.25C8.15898 1.25 9.16732 2.25417 9.16732 3.54167C9.16732 5.1125 7.75065 6.4 5.60482 8.34583L5.00065 8.89583Z" fill="#E93C3C"/>
            </svg>
            <p>${data.like}</p>
          </div>

          <div class="bun" id="view">
            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12" fill="none">
              <path d="M6 4.5C5.60218 4.5 5.22064 4.65804 4.93934 4.93934C4.65804 5.22064 4.5 5.60218 4.5 6C4.5 6.39782 4.65804 6.77936 4.93934 7.06066C5.22064 7.34196 5.60218 7.5 6 7.5C6.39782 7.5 6.77936 7.34196 7.06066 7.06066C7.34196 6.77936 7.5 6.39782 7.5 6C7.5 5.60218 7.34196 5.22064 7.06066 4.93934C6.77936 4.65804 6.39782 4.5 6 4.5ZM6 8.5C5.33696 8.5 4.70107 8.23661 4.23223 7.76777C3.76339 7.29893 3.5 6.66304 3.5 6C3.5 5.33696 3.76339 4.70107 4.23223 4.23223C4.70107 3.76339 5.33696 3.5 6 3.5C6.66304 3.5 7.29893 3.76339 7.76777 4.23223C8.23661 4.70107 8.5 5.33696 8.5 6C8.5 6.66304 8.23661 7.29893 7.76777 7.76777C7.29893 8.23661 6.66304 8.5 6 8.5ZM6 2.25C3.5 2.25 1.365 3.805 0.5 6C1.365 8.195 3.5 9.75 6 9.75C8.5 9.75 10.635 8.195 11.5 6C10.635 3.805 8.5 2.25 6 2.25Z" fill="#222324"/>
            </svg>
            <p>${data.view}</p>
          </div>
        </div>
      </div>


      <div class="second-linecard">
        <p>${data.text}
        </p>
        <div class="readmore">
        <a href="${data.link}">อ่านต่อ</a>
       </div>
      </div>

      <div class="third-linecard">
        <div class="activity">
          <h3>กิจกรรมในทริป :</h3>
        </div>
        <div class="num-act">
          <p>${data.activity}</p>
        </div>
      </div>

    </div>
  </div>
    `;
  }
  blog.innerHTML = innerHTML;

  // Ensure that the first radio button is checked by default
  var firstRadioBtn = document.querySelector("input[name=radio-btn]");
  firstRadioBtn.checked = true;
})(jQuery);
