(function ($) {
  var myBlog = [
    {
      pic: "chonburi.png",
      text: "ชลบุรี"
    },
    {
      pic: "prajin.png",
      text: "ปราจีนบุรี"
    },
    {
      pic: "bangkok.png",
      text: "กรุงเทพ"
    }
  ];
  var blog = document.getElementById("CountryList");
  // ref to id CountryList
  var innerHTML = "";
  for (let i = 0; i < myBlog.length; i++) {
    let data = myBlog[i];
    innerHTML += `
    <a href="#" class="country" id="c-${i}">
        <img src="${data.pic}" alt="">
        <h2>${data.text}</h2>
      </a>
    `;
  }
  blog.innerHTML = innerHTML;

  // Ensure that the first radio button is checked by default
  var firstRadioBtn = document.querySelector("input[name=radio-btn]");
  firstRadioBtn.checked = true;
})(jQuery);
